package models

import "time"

type Achievement struct {
	Object_type  string    `json:"type"`
	Id           int       `json:"id"`
	Tier_id      int       `json:"tier_id"`
	Range        string    `json:"range"`
	Kpm_id       int       `json:"kpm_id"`
	Customer_id  int       `json:"customer_id"`
	Name         string    `json:"name"`
	Description  string    `json:"description"`
	Measure      string    `json:"measure"`
	Badge_id     int       `json:"badge_id"`
	Points       int       `json:"points"`
	Value        float64   `json:"value"`
	Team_id      int       `json:"team_id"`
	Display_name string    `json:"display_name"`
	Level        int       `json:"level"`
	Date_earned  time.Time `json:"date_earned"`
}

type TeamAchievements struct {
	Achievements []Achievement
	Kpm_id       int
	Team_id      int
	Customer_id  int
}

type EarnedAchievements struct {
	Achievements []Achievement
}
