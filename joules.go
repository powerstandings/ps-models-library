package models

import (
	"time"
)

type WorkRequest struct {
	External_record_type string    `json:"external_record_type"`
	External_record_id   int       `json:"external_record_id"`
	User_id              int       `json:"user_id"`
	Billing_user_id      int       `json:"billing_user_id"`
	Activity_type        *string   `json:"activity_type"`
	Kpm_id               int       `json:"kpm_id"`
	Joule_count          int       `json:"joule_count"`
	Data_value           float64   `json:"data_value"`
	Duration             string    `json:"duration"`
	Datetime             time.Time `json:"datetime"`
	Activity_id          int       `json:"activity_id"`
	Reference_id         int       `json:"reference_id"`
	Requeues             int       `json:"requeues"`
	Reason               string    `json:"kpi_totals_adjuster_reason"`
	Actor_id  			 int       `json:"actor_id"`
	Kpi_totals_adjuster  bool      `json:"kpi_totals_adjuster_joule"`
	Offset_location       string    `json:"offset_location"`
}

type Joule struct {
	User_id               		int       `json:"user_id"`
	Team_id               		int       `json:"team_id"`
	Customer_id           		int       `json:"customer_id"`
	Billing_user_id       		int       `json:"billing_user_id"`
	Created_user_id       		int       `json:"created_by_user_id"`
	Isdc_points           		int       `json:"isdc_points"`
	Power_points          		float64   `json:"power_points"`
	Calculated_points     		float64   `json:"calculated_points"`
	Kpm_id                		int       `json:"kpm_id"`
	Reference_id          		int       `json:"reference_id"`
	Processed             		int       `json:"processed"`
	Joule_count           		int       `json:"joule_count"`
	Data_value            		float64   `json:"data_value"`
	Duration              		string    `json:"duration"`
	Date_created          		time.Time `json:"datetime"`
	Requeues              		int       `json:"requeues"`
	Joule_processed       		bool      `json:"joule_processed"`
	Achievement_processed 		bool      `json:"achievement_processed"`
	KPI_processed         		bool      `json:"kpi_processed"`
	Flush_joules          		bool      `json:"flush_joules"`
	Builder               		bool      `json:"builder"`
	Tracer                		int       `json:"tracer"`
	Offset_location           	string    `json:"offset_location"`
	Reason                		string    `json:"kpi_totals_adjuster_reason"`
	Actor_id  					int       `json:"actor_id"`
	Kpi_totals_adjuster  		bool      `json:"kpi_totals_adjuster_joule"`
	KPM                   		Kpm
	UserInfo             		User
	PowerPoints           		PowerPoint
	UserTotals            		map[string]int64
	PointsTotals          		map[string]int64
}
