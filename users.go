package models

type User struct {
	User_id     int
	Team_id     int
	Customer_id int
	First_name  string
	Last_name   string
	Offset_location   string
}
