package models

// category
const EFFORT string = "Effort"
const LEAD_GEN string = "Lead Gen"
const SALES string = "Sales"
const POWERPOINT_ADJUSTMENT string = "Standings Point Adjustment"

// display type
const INTEGER string = "integer"
const TIME string = "time"
const CURRENCY string = "currency"
const PERCENT string = "percent"

// key values
const DIAL string = "dials"
const OUTBOUND_TALK_TIME string = "outtalk"
const VOICEMAIL string = "vms"
const EMAIL string = "emails"
const FAX string = "faxes"
const SMS string = "sms"
const SOCIAL_MSG string = "social"
const INBOUND_AVAILABLE_TIME string = "inbdavl"
const INBOUND_TALK_TIME string = "inbdtalk"
const SET_CALLBACK string = "callbacks"
const TOTAL_TALK_TIME string = "totaltalk"
const LEAD_RESPONSE_TIME string = "rsptime"
const CONTACT string = "contacts"
const CORRECT_CONTACT string = "corcntcts"
const APPOINTMENT_SET string = "apptsset"
const APPOINTMENT_HELD string = "apptsheld"
const QUALIFIED_OPPORTUNITY string = "qualopp"
const DISQUALIFIED_LEAD string = "disqlld"
const COMPLETED_NEEDS_ANALYSIS string = "ndsanly"
const COMPLETED_PRODUCT_DEMONSTRATION string = "demos"
const PROPOSAL_SENT string = "propsnt"
const CONTRACT_REVIEWED string = "ctrctrev"
const OPPORTUNITY_SIZE string = "oppsize"
const OPPORTUNITY_WON string = "oppwon"
const OPPORTUNITY_WON_SIZE string = "wonsize"
const OPPORTUNITY_LOST string = "opplost"
const ONE_TIME_REVENUE string = "one_time_rev"
const MONTHLY_RECURRING_REVENUE string = "mon_rec_rev"
const ANNUAL_CONTRACT_VALUE string = "annual_con_val"
const TOTAL_CONTRACT_VALUE string = "bookings"
const THROWDOWN_ACCEPTED string = "thrwdwn_accept"
const THROWDOWN_WON string = "thrwdwn_won"
const THROWDOWN_REJECTED string = "thrwdwn_rjctd"
const THROWDOWN_ISSUED string = "thrwdwn_issued"
const CHALLENGE_WON string = "challenge_won"
const CHALLENGE_LEADER string = "challenge_ldr"
const CHALLENGE_START string = "challenge_start"
const CHALLENGE_END string = "challenge_end"
const ACHIEVEMENT_WON string = "achievement_won"
const LEADERLIST_LEADER string = "kpi_ldr"

const TODAY = "today"
const THIS_WEEK = "this_week"
const THIS_MONTH = "this_month"
const THIS_QUARTER = "this_qrtr"

var EVENT_KPMS = [...]string{
	APPOINTMENT_SET,
	APPOINTMENT_HELD,
	COMPLETED_NEEDS_ANALYSIS,
	DISQUALIFIED_LEAD,
	MONTHLY_RECURRING_REVENUE,
	ONE_TIME_REVENUE,
	OPPORTUNITY_LOST,
	OPPORTUNITY_WON,
	PROPOSAL_SENT,
	QUALIFIED_OPPORTUNITY,
	TOTAL_CONTRACT_VALUE,
}

var ALL_RANGES = [...]string{
	TODAY,
	THIS_WEEK,
	THIS_MONTH,
	THIS_QUARTER,
}
