package models

type Teams struct {
	Object_type string `json:"type"`
	Team_id     int    `json:"team_id"`
	Team_name   string `json:"name"`
	Members     []int  `json:"members"`
}
