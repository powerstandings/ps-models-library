package models

// Standard notification socket message example:
//
//{
//     "body": "You have passed all other agents to become the leader of total Emails made for Today",
//     "send_to_stomp_manager_time_stamp": 1437765722,
//     "_id": {
//         "$id": "55b2905a718488a9318b4567"
//     },
//     "user_id": 20143009,
//     "title": "You are now the Emails leader!",
//     "read": false,
//     "data": [],
//     "topic": "notifications",
//     "destination_id": 20143009,
//     "customer_id": 20073709,
//     "message_type": "_first_place",
//     "unread": {
//         "count": 162
//     }
// }

type SocketMessage interface{}

type UnreadCount struct {
	Count int `json:"count"`
}

type NotificationSocketMessage struct {
	Body                             string        `json:"body"`
	Send_to_stomp_manager_time_stamp int           `json:"send_to_stomp_manager_timestamp"`
	User_id                          int           `json:"user_id"`
	Title                            string        `json:"title"`
	Read                             bool          `json:"read"`
	Data                             []interface{} `json:"data"`
	Topic                            string        `json:"topic"`
	Destination_id                   string        `json:"destination_id"`
	Customer_id                      int           `json:"customer_id"`
	Message_type                     string        `json:"message_type"`
	Unread                           UnreadCount   `json:"unread"`
	Exchange                         string        `json:"exchange"`
	Picture_user_id 				 int    	   `json:"picture_user_id"`
}

type SocketBody struct {
	Title          string `json:"title"`
	User           string `json:"user"`
	Subtitle       string `json:"subtitle"`
	Message_type   string `json:"type"`
	Kpm_id         string `json:"kpm_id"`
	Team_ids       []int  `json:"team_ids"`
	Leaderboard_id int    `json:"leaderboard_id"`
	User_id        int    `json:"user_id"`
}

type LeaderboardSocketMessage struct {
	Topic                            string     `json:"topic"`
	Body                             SocketBody `json:"body"`
	Send_to_stomp_manager_time_stamp int64      `json:"send_to_stomp_manager_time_stamp"`
	Customer_id                      []int      `json:"customer_id"`
	User_ids                         []int      `json:"user_ids"`
	Destination_id                   string     `json:"destination_id"`
	Read                             bool       `json:"read"`
	Exchange                         string     `json:"exchange"`
}

type RefreshSocketMessage struct {
	Topic                            string `json:"topic"`
	Send_to_stomp_manager_time_stamp int64  `json:"send_to_stomp_manager_time_stamp"`
	Customer_id                      int    `json:"customer_id"`
	User_id                          string `json:"user_id"`
	Destination_id                   string `json:"destination_id"`
	Body                             string `json:"body"`
	Exchange                         string `json:"exchange"`
}

type Message struct {
	Id           uint64 `json:"id"`
	Body         string `json:"body"`
	Title        string `json:"title"`
	Message_type string `json:"message_type"`
}

type MessageCollection struct {
	Messages []Message
}
