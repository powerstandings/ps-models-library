package models

import "time"

type Challenge struct {
	Object_type         string           `json:"type"`
	Id                  int              `json:"id"`
	Customer_id         int              `json:"customer_id"`
	Name                string           `json:"name"`
	Versus_type         string           `json:"versus_type"`
	Game_type           string           `json:"game_type"`
	Start_date          time.Time        `json:"start_date"`
	End_date            time.Time        `json:"end_date"`
	Status              string           `json:"status"`
	Description         string           `json:"description"`
	Operator            string           `json:"operator"`
	Value               float64          `json:"value"`
	Kpm_id              int              `json:"kpm_id"`
	Recurring_type      string           `json:"recurring_type"`
	Recurring_start_day string           `json:"recurring_start_day"`
	Recurring_end_day   string           `json:"recurring_end_day"`
	Created_by          int              `json:"created_by"`
	Modified_by         int              `json:"modified_by"`
	Date_created        time.Time        `json:"date_created"`
	Date_modified       time.Time        `json:"date_modified"`
	Challenge_teams     []int            `json:"challenge_teams"`
	Challenge_points    []ChallengePoint `json:"challenge_points"`
}

type ChallengePoint struct {
	Object_type  string `json:"type"`
	Customer_id  int    `json:"customer_id"`
	Challenge_id int    `json:"challenge_id"`
	Points       int    `json:"points"`
	Rank         int    `json:"rank"`
}

type ChallengeStore struct {
	Object_type  string    `json:"type"`
	Customer_id  int       `json:"customer_id"`
	Kpm_id       int       `json:"kpm_id"`
	Challenge_id int       `json:"challenge_id"`
	Challenge    Challenge `json:"challenge"`
	Ranks        []Rank    `json:"ranks"`
}

type ChallengeStores struct {
	Object_type string   `json:"type"`
	Store_keys  []string `json:"store_keys"`
}
