package models

type Kpm struct {
	Id                int
	Active            int
	Isdc_points       int
	Customer_id       int
	Category          string
	Name              string
	Kpm_name          string
	Label_name        string
	Name_plural       string
	Kpm_name_plural   string
	Label_name_plural string
	Description       string
	Abbr              string
	Kpm_abbr          string
	Label_abbr        string
	Key_value         string
	Display_type      string
}
