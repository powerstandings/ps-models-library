package models

import (
	"time"
)

type PointsStore struct {
	Object_type string     `json:"type"`
	Customer_id int        `json:"customer_id"`
	Indicator   string     `json:"indicator"`
	Dashboards  [][]string `json:"dashboards"`
	Details     []Rank     `json:"details"`
}

type PowerPoint struct {
	Id            int
	Kpm_id        int
	Customer_id   int
	Points        float64
	Denominator   int
	Label         string
	Url           string
	Active        int
	Date_created  time.Time
	Date_modified time.Time
}
