package models

type Subscriptions struct {
	Object_type    string `json:"type"`
	Last_update    int64  `json:"last_update"`
	Dashboards     []int  `json:"dashboards"`
	Standings_bars []int  `json:"standings_bars"`
	Leaderlists    []int  `json:"leaderlists"`
	Events         []int  `json:"events"`
	Notifications  []int  `json:"notifications"`
	Psn            []int  `json:"psn"`
}
