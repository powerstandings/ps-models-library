package models

type KpiStore struct {
	Object_type    string `json:"type"`
	Team_ids       []int  `json:"team_ids"`
	Customer_id    int    `json:"customer_id"`
	Kpm_id         int    `json:"kpm_id"`
	Store_id       int    `json:"id"`
	Indicator      string `json:"indicator"`
	Standings_bars []int  `json:"standings_bars"`
	// Dashboards is an array of arrays. The inner array
	// is [dashboard_id, widget_id] so dashboard[0] is the
	// id of the dashboard, and dashboard[1] is the id of
	// the individual dashboard widget.
	Dashboards [][]string `json:"dashboards"`
	Rankings   []Rank     `json:"details"`
}

type KpiStores struct {
	Store_ids []int `json:"store_ids"`
}
