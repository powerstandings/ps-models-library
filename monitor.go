package models

import (
	"time"
)

type Monitor struct {
	System struct {
		Process_id        int    `json:"process_id"`
		Parent_process_id int    `json:"parent_process_id"`
		Hostname          string `json:"hostname"`
		Directory         string `json:"directory"`
		Number_cpus       int    `json:"number_cpus"`
		Goroutines        int    `json:"goroutines"`
		Go_max_procs      int    `json:"go_max_procs"`
		Go_version        string `json:"go_version"`
		PPI               uint64 `json:"psn_performance_index"`
	}

	Uptime struct {
		Started        time.Time `json:"started"`
		Uptime_seconds float64   `json:"uptime_seconds"`
		Uptime_hours   float64   `json:"uptime_hours"`
		Uptime_minutes float64   `json:"uptime_minutes"`
		Uptime_string  string    `json:"uptime_string"`
	}

	Joules MonitorJoules

	Socket struct {
		Notification_success uint64 `json:"notification_success"`
		Notification_failed  uint64 `json:"notification_failed"`
	}

	Cache MonitorCache

	APIServer struct {
		Requests             uint64 `json:"total_requests"`
		KPIStore_requests    uint64 `json:"kpi_store_requests"`
		PointsStore_requests uint64 `json:"points_store_requests"`
	}
}

type APIResponse struct {
	Status  string
	Message string
}

type MonitorJoules struct {
	Joules_recieved        uint64 `json:"joules_recieved"`
	Context_processed      uint64 `json:"context_processed"`
	Counts_processed       uint64 `json:"counts_processed"`
	Points_processed       uint64 `json:"points_processed"`
	Joules_stored          uint64 `json:"joules_stored"`
	Widgets_processed      uint64 `json:"widgets_processed"`
	Achievements_processed uint64 `json:"achievements_processed"`
	Challenges_processed   uint64 `json:"challenges_processed"`
	Joules_acked           uint64 `json:"joules_acked"`
	Requeues               uint64 `json:"requeues"`
	Multiple_requeues      uint64 `json:"multiple_requeues"`
	Requeues_dropped       uint64 `json:"requeues_dropped"`
	Joules_dropped         uint64 `json:"joules_dropped"`
	Joules_in_batch        uint64 `json:"joules_in_batch"`
	Power_points_earned    uint64 `json:"power_points_earned"`
}

type MonitorCache struct {
	Item_count int `json:"cache.item_count"`
}
