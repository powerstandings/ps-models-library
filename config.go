package models

type Config struct {
	Configuration_file string
	Database           struct {
		Powerstandings_read_connection struct {
			Connections          []string
			Max_open_connections int
			Max_idle_connections int
		}
		Powerstandings_write_connection struct {
			Connections          []string
			Max_open_connections int
			Max_idle_connections int
		}
		Billing_read_connection struct {
			Connections          []string
			Max_open_connections int
			Max_idle_connections int
		}
		Joules_storage_connection struct {
			Connections          []string
			Max_open_connections int
			Max_idle_connections int
		}
	}
	Couchbase struct {
		Couchbase_enabled         bool
		Couchbase_connection      string
		Couchbase_username        string
		Couchbase_password        string
		Couchbase_bucket          string
		Couchbase_bucket_password string
		Couchbase_design_docs     string
	}
	Queue struct {
		Queue_connection string
		Queue_name       string
		Auto_ack         bool
	}
	Notifications struct {
		Connection             string
		Notifications_exchange string
		Dashboard_exchange     string
		Standings_exchange     string
		Leaderboard_exchange   string
		Routers                []string
		Routers_realm          string
	}
	Settings struct {
		Workers                  int
		Cache_expiration         int
		Cache_cleanup            int
		Max_batch_size           int
		Min_batch_size           int
		Max_wait_time            int
		Max_joule_retries        int
		Monitor_port             string
		Max_cores                int
		Prefetch                 int
		Subscriptions_enabled    bool  `json:"subscriptions_enabled"`
		Subscriptions_expiration int   `json:"subscriptions_expiration"`
		Subscriptions_stale      int64 `json:"subscriptions_stale"`
	}
}

type PrintConfig struct {
	Database struct {
		Powerstandings_read_connection struct {
			Connections          []string `json:"connections"`
			Max_idle_connections int      `json:"max_idle_connections"`
			Max_open_connections int      `json:"max_open_connections"`
		}
		Powerstandings_write_connection struct {
			Connections          []string `json:"connections"`
			Max_idle_connections int      `json:"max_idle_connections"`
			Max_open_connections int      `json:"max_open_connections"`
		}
		Billing_read_connection struct {
			Connections          []string `json:"connections"`
			Max_idle_connections int      `json:"max_idle_connections"`
			Max_open_connections int      `json:"max_open_connections"`
		}
		Joules_storage_connection struct {
			Connections          []string `json:"connections"`
			Max_idle_connections int      `json:"max_idle_connections"`
			Max_open_connections int      `json:"max_open_connections"`
		}
	}
	Couchbase struct {
		Couchbase_enabled         bool   `json:"couchbase_enabled"`
		Couchbase_connection      string `json:"couchbase_connection"`
		Couchbase_bucket          string `json:"couchbase_bucket"`
		Couchbase_bucket_password string `json:"couchbase_bucket_password"`
		Couchbase_username        string `json:"couchbase_username"`
		Couchbase_password        string `json:"couchbase_password"`
		Couchbase_design_docs     string `json:"couchbase_design_docs"`
	}
	Queue struct {
		Queue_connection string `json:"queue_connection"`
		Queue_name       string `json:"queue_name"`
		Auto_ack         bool   `json:"auto_ack"`
	}
	Notifications struct {
		Connection             string   `json:"connection"`
		Notifications_exchange string   `json:"notifications_exchange"`
		Dashboard_exchange     string   `json:"dashboard_exchange"`
		Standings_exchange     string   `json:"standings_exchange"`
		Leaderboard_exchange   string   `json:"leaderboard_exchange"`
		Routers                []string `json:"routers"`
		Routers_realm          string   `json:"routers_realm"`
	}
	Settings struct {
		Workers                  int    `json:"workers"`
		Cache_expiration         int    `json:"cache_expiration"`
		Cache_cleanup            int    `json:"cache_cleanup"`
		Max_batch_size           int    `json:"max_batch_size"`
		Min_batch_size           int    `json:"min_batch_size"`
		Max_wait_time            int    `json:"max_wait_time"`
		Max_joule_retries        int    `json:"max_joule_retries"`
		Monitor_port             string `json:"monitor_port"`
		Max_cores                int    `json:"max_cores"`
		Prefetch                 int    `json:"prefetch"`
		Subscriptions_enabled    bool   `json:"subscription_manager_enabled"`
		Subscriptions_expiration int    `json:"subscription_cache_expiration"`
		Subscriptions_stale      int64  `json:"subscriptions_stale"`
	}
}
